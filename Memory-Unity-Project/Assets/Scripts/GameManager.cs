﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Analytics;

public class GameManager : MonoBehaviour
{
    public static int CurrentLevelNumber;

    //allows the ToString function to represent Symbols by name. for example Symbols.Waves.ToString()= "Waves" instead of "0"
    [System.Flags]
    //types of symbols/cards
    public enum Symbols
    {
        Waves =             1 << 0,
        Dot =               1 << 1,
        Square =            1 << 2,
        LargeDiamond =      1 << 3,
        SmallDiamonds =     1 << 4,
        Command =           1 << 5,
        Bomb =              1 << 6,
        Sun =               1 << 7,
        Bones =             1 << 8,
        Drop =              1 << 9,
        Face =              1 << 10,
        Hand =              1 << 11,
        Flag =              1 << 12,
        Disk =              1 << 13,
        Candle =            1 << 14,
        Wheel =             1 << 15
    }

    // creates a section in the inspector that can be manipulated
    [System.Serializable]
    // determines Rows and Collumns of Cards and which cards are to be used
    public struct LevelDescription
    {
        public int Rows, Columns;
        [EnumFlags]
        public Symbols UsedSymbols;
    }

    public GameObject TilePrefab;
    public float TileSpacing;

    public LevelDescription[] Levels = new LevelDescription[0];
    public Action HideTilesEvent;

    private Tile m_tileOne, m_tileTwo;
    private int m_cardsRemaining;

    int totalMistakesMade = 0;
    int mistakesMade = 0;

    int matchMade = 0;
    int totalMatchesMade = 0;

    float timeSinceLastTile=0;





    private void Awake()
    {
        totalMistakesMade = 0;
        totalMatchesMade = 0;

        if (CurrentLevelNumber == 0) { 
        Analytics.CustomEvent("GameStart", new Dictionary<string, object>{
            {"gameStarted",0 }
        });
        }
    }

    private void Start()
    {

        LoadLevel( CurrentLevelNumber );
        timeSinceLastTile = 0;

    }


    private void Update()
    {
        timeSinceLastTile += Time.deltaTime;


    }

    //loads level based on element number in the list of levels
    private void LoadLevel( int levelNumber )
    {
        matchMade = 0;
        mistakesMade = 0;
        //sets up level description based on which level it is in the list of Levels in the inspector.  
        LevelDescription level = Levels[levelNumber % Levels.Length];

        //gets the symbols to use in game from the inspector and then populates a goes 
        List<Symbols> symbols = GetRequiredSymbols( level );

        //instantiates tile game objects positioned based on their associated row and column numbers + tile spacing
        //randomly assigns a corresponding symbol to the material 
        for ( int rowIndex = 0; rowIndex < level.Rows; ++rowIndex )
        {
            float yPosition = rowIndex * ( 1 + TileSpacing );
            for ( int colIndex = 0; colIndex < level.Columns; ++colIndex )
            {
                float xPosition = colIndex * ( 1 + TileSpacing );
                // instantiate a tile at the row and column index
                GameObject tileObject = Instantiate( TilePrefab, new Vector3( xPosition, yPosition, 0 ), Quaternion.identity, this.transform );
                // get a random symbol from symbols list.
                int symbolIndex = UnityEngine.Random.Range( 0, symbols.Count );
                //adjust the material on the childs renderer to show the appropriate symbol
                tileObject.GetComponentInChildren<Renderer>().material.mainTextureOffset = GetOffsetFromSymbol( symbols[symbolIndex] );
                //give the tile gameobject the gamemanager and the symbol it has
                tileObject.GetComponent<Tile>().Initialize( this, symbols[symbolIndex] );
                //remove the symbol from the list so it cant be used again on a different tile.
                symbols.RemoveAt( symbolIndex );
            }
        }
        //center the camera and position the camera
        SetupCamera( level );
    }

    //creates a new list of type symbols of which Used Symbols are allowed
    private List<Symbols> GetRequiredSymbols( LevelDescription level )
    {
        List<Symbols> symbols = new List<Symbols>();
        int cardTotal = level.Rows * level.Columns;

        // gets the values of all symbols and puts them in an array
        {
            Array allSymbols = Enum.GetValues( typeof( Symbols ) );

            m_cardsRemaining = cardTotal;

            //checks if the total number of cards in the level is even
            if ( cardTotal % 2 > 0 )
            {
                new ArgumentException( "There must be an even number of cards" );
            }

            //if theyre listed in UsedSymbols then add them to the list symbols
            foreach ( Symbols symbol in allSymbols )
            {
                if ( ( level.UsedSymbols & symbol ) > 0 )
                {
                    symbols.Add( symbol );
                }
            }
        }

        //if there are no symbols or if there are more symbols than pairs of cards throw an exception
        {
            if ( symbols.Count == 0 )
            {
                new ArgumentException( "The level has no symbols set" );
            }
            if ( symbols.Count > cardTotal / 2 )
            {
                new ArgumentException( "There are too many symbols for the number of cards." );
            }
        }

        // determines how many pairs of cards will have to repeat using symbols (pairs - usable symbols = # of repeated symbols)
        {
            int repeatCount = ( cardTotal / 2 ) - symbols.Count;
            //if there are repeats of a symbol
            if ( repeatCount > 0 )
            {
                //creates a copy of the list symbols and creates an empty list
                List<Symbols> symbolsCopy = new List<Symbols>( symbols );
                List<Symbols> duplicateSymbols = new List<Symbols>();

                // for every pair of repeats
                for ( int repeatIndex = 0; repeatIndex < repeatCount; ++repeatIndex )
                {

                    //selects a random symbol from symbolsCopy
                    int randomIndex = UnityEngine.Random.Range( 0, symbolsCopy.Count );
                    // adds it to duplicateSymbols (repeats) and removes it from symbols copy (so not to repeat it again)
                    duplicateSymbols.Add( symbolsCopy[randomIndex] );
                    symbolsCopy.RemoveAt( randomIndex );

                    //if symbols copy empties (all symbols have been repeated) repopulate symbols copy with the list of usable symbols
                    if ( symbolsCopy.Count == 0 )
                    {
                        symbolsCopy.AddRange( symbols );
                    }
                }
                //adds the list of repeated symbols to symbols.
                symbols.AddRange( duplicateSymbols );
            }
        }
        // copys the entire list of symbols and adds it to the list
        symbols.AddRange( symbols );

        return symbols;
    }

    // determines the symbol and returns a vector 2 corresponding to that symbols position in the texture image
    private Vector2 GetOffsetFromSymbol( Symbols symbol )
    {
        Array symbols = Enum.GetValues( typeof( Symbols ) );
        for ( int symbolIndex = 0; symbolIndex < symbols.Length; ++symbolIndex )
        {
            if ( ( Symbols )symbols.GetValue( symbolIndex ) == symbol )
            {
                return new Vector2( symbolIndex % 4, symbolIndex / 4 ) / 4f;
            }
        }
        Debug.Log( "Failed to find symbol" );
        return Vector2.zero;
    }

    // positions the camera to fit all rows and columns  and then positions the camera in the center, 10 units away
    private void SetupCamera( LevelDescription level )
    {
        Camera.main.orthographicSize = ( level.Rows + ( level.Rows + 1 ) * TileSpacing ) / 2;
        Camera.main.transform.position = new Vector3( ( level.Columns * ( 1 + TileSpacing ) ) / 2, ( level.Rows * ( 1 + TileSpacing ) ) / 2, -10 );
    }

    //if 2 tiles have been clicked and the symbols both match  set waitForHides value "match" to true and call coroutine, otherwise call it anyway while false.
    public void TileSelected(Tile tile)
    {

        Analytics.CustomEvent("TimeSince", new Dictionary<string, object> { 
        { "timePassedSinceLastClick:", timeSinceLastTile  } });
        timeSinceLastTile = 0;



        if ( m_tileOne == null )
        {
            m_tileOne = tile;
            m_tileOne.Reveal();
        }
        else if ( m_tileTwo == null )
        {
            m_tileTwo = tile;
            m_tileTwo.Reveal();
            if ( m_tileOne.Symbol == m_tileTwo.Symbol )
            {
                StartCoroutine( WaitForHide( true, 1f ) );

                totalMatchesMade++;
                matchMade++;

                Analytics.CustomEvent("MatchMade", new Dictionary<string, object> {
                    { "match: ", matchMade   },
                    { "level: ",CurrentLevelNumber },
                    {"totalMatches: ",totalMatchesMade  }
                });

            }
            else
            {
                StartCoroutine( WaitForHide( false, 1f ) );

                mistakesMade++;
                totalMistakesMade++;

                Analytics.CustomEvent("MistakeMade", new Dictionary<string, object> {
                    { "mistake: ", mistakesMade   },
                    { "level: ",CurrentLevelNumber },
                    {"totalMistakes: ",totalMistakesMade  }
                });
            }
        }
    }
    
    // increment the currentlevelnumber up by one on completion, and load a new scene with the correct tile setup, otherwise if there are no more levels, end the game.
    private void LevelComplete()
    {
        Analytics.CustomEvent("LevelComplete", new Dictionary<string, object> {
            { "level: ", CurrentLevelNumber }});

        ++CurrentLevelNumber;
        if (CurrentLevelNumber > Levels.Length - 1)
        {
            Debug.Log("GameOver");
        }
        else
        {
            SceneManager.LoadScene( SceneManager.GetActiveScene().buildIndex );
        }
    }

    // wait for "time"  and destroy both clicked on tiles if they are a match, else hide them again. once there are no more cards, call level complete.
    private IEnumerator WaitForHide( bool match, float time )
    {
        float timer = 0;
        while ( timer < time )
        {
            timer += Time.deltaTime;
            if ( timer >= time )
            {
                break;
            }
            yield return null;
        }
        if ( match )
        {
            Destroy( m_tileOne.gameObject );
            Destroy( m_tileTwo.gameObject );
            m_cardsRemaining -= 2;
        }
        else
        {
            m_tileOne.Hide();
            m_tileTwo.Hide();
        }
        m_tileOne = null;
        m_tileTwo = null;

        if ( m_cardsRemaining == 0 )
        {
            LevelComplete();
        }
    }


}
